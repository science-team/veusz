veusz (3.6.2-1) unstable; urgency=medium

  * Team upload.

  * [44bc918] Remove too old package: veusz-helpers from breaks-replaces
  * [58358b7] Update .gitlab-ci.yml

  [ Jeremy Sanders ]
  * Update to Veusz 3.6.2
  * Remove upstreamed patch for Numpy test failures

 -- Anton Gladky <gladk@debian.org>  Mon, 20 Mar 2023 07:10:18 +0100

veusz (3.5.3-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Update gbp.conf to use pristine-tar & --source-only-changes by default.
  * Add upstream patch to fix test failure with Numpy 1.24 (closes: #1029073).

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 18 Jan 2023 13:33:57 +0400

veusz (3.5.3-1) unstable; urgency=low

  * Update to Veusz 3.5.3 (Closes: #1023185)
  * Update Standards-Version to 4.6.1
  * Remove upstreamed patches (sipbuild API and sip6 build system)
  * Remove source causing lintian error source-is-missing
  * Clean up rules

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sun, 06 Nov 2022 13:55:00 +0300

veusz (3.3.1-3) unstable; urgency=medium

  * Team upload.
  * Backport upstream changes to fix FTBFS with sip 6.2 (closes: #997526).

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 07 Nov 2021 12:37:04 +0300

veusz (3.3.1-2) unstable; urgency=medium

  * Team upload.
  * Add a patch to build using sipbuild API instead of deprecated sip5 tool.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 15 Aug 2021 17:50:51 +0300

veusz (3.3.1-1) unstable; urgency=low

  * Update to Veusz 3.3.1
  * Bump Standards-Version to 4.5.1
  * Include upstream GPG signing key
  * Drop upstreamed sip5 compatibility patches
  * Increase debhelper-compat level to 13

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Wed, 16 Dec 2020 14:35:00 +0100

veusz (3.2.1-2) unstable; urgency=medium

  * Team upload.
  * Port from sip4 to sip5 (closes: #964133, #971172).
    - Add three patches that were accepted upstream.
    - Update build- and run-time dependencies.
    - Export SIP_EXE=sip5 in debian/rules.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 28 Sep 2020 21:56:32 +0300

veusz (3.2.1-1) unstable; urgency=low

  [ Jeremy Sanders ]
  * Update to Veusz 3.2.1 (Closes: #945467)
  * Standards-Version: 4.5.0
  * Copy icon files rather than symlink, to fix icon-not-found appsteam
    generator error
  * Fix glob for .so files in python3-veusz.helpers
  * Fix linitan warnings for rules-requires-root,
    uses-debhelper-compat-file, sphinxdoc-but-no-sphinxdoc-depends,
    duplicate-short-description

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sun, 17 May 2020 11:12:00 +0200

veusz (3.0.1-1) unstable; urgency=low

  [ Jeremy Sanders ]
  * Update to Veusz 3.0.1
  * Remove build xvfb dependency
  * Add breaks+replaces for veusz and veusz-helpers << 3.0-1 (closes: #903450)
  * Use qtchooser method for finding qmake

  [ Andreas Tille ]
  * Add autopkgtest
  * debhelper 12
  * Standards-Version: 4.3.0
  * Remove trailing whitespace in debian/copyright

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Fri, 08 Feb 2019 14:53:37 +0100

veusz (3.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Jeremy Sanders ]
  * Update to Veusz 3.0
  * d/copyright: Update URLs
  * d/control: Update descriptions
  * d/control: Update standards version to 4.1.4
  * d/patches: Removed 01-numpy-1.13.diff
  * d/rules: Fix for self tests for newer pybuild versions
  * Switch from Python 2 to 3 and provide a python3-veusz subpackage

  [ Andreas Tille ]
  * hardening=+all
  * Fix capitalisation in description
  * Fix path in d/copyright
  * Fix short description
  * Move packaging to Debian Science team

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sat, 16 Jun 2018 10:48:00 +0200

veusz (2.0.1-1) unstable; urgency=low

  * Update to Veusz 2.0.1 which uses Qt5 (closes: #875232)
  * Split out module into python3-veusz and python-veusz packages
  * Rename veusz-helpers to python*-veusz.helpers
  * Split out common data into veusz-data

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sat, 30 Sep 2017 11:45:00 +0200

veusz (1.21.1-1) unstable; urgency=low

  * Update to Veusz 1.21.1
  * Remove arm self test patch

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sun, 31 Aug 2014 15:11:00 +0200

veusz (1.20.1-3) unstable; urgency=medium

  * Team upload.
  * Add Breaks+Replaces relationship on veusz-helpers (<< 1.20.1-3) to fix
    wheezy->jessie upgrade path. (Closes: #714891)

 -- Vincent Cheng <vcheng@debian.org>  Sat, 08 Mar 2014 23:33:45 -0800

veusz (1.20.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild in a clean sid environment for correct sip-api-10.0 dependency.
    (Closes: #739467, #740925)

 -- Vincent Cheng <vcheng@debian.org>  Thu, 06 Mar 2014 09:52:43 -0800

veusz (1.20.1-1) unstable; urgency=low

  [Jakub Wilk]
  * Use canonical URIs for Vcs-* fields.

  [Jeremy Sanders]
  * Update to Veusz 1.20.1
  * Update Standards-Version to 3.9.5
  * Remove workaround for #589759
  * Remove patch to work around ARM test failures, as fixed upstream
  * Add patch for a second ARM self build failure (pushed upstream)
  * Install package-provided .desktop and mime database files
  * Modify documentation generation dependency to fop and xmlto
  * Add suggests for python-dbus and python-h5py
  * Increase minimum python version to 2.6
  * Add verification of signing key in watch and add upstream key
  * Update entries in copyright file

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sun, 16 Feb 2014 14:25:00 +0100

veusz (1.15-1) unstable; urgency=low

  * Update to upstream Veusz 1.15
  * Run dh_numpy to get numpy API dependeny
  * Run dh_sip and add sip:Depends dependency to get sip API
  * Replace patches with use of upstream setup.py options and symlinks
  * Bumped Standards-Version to 3.9.3
  * Do not run incorrectly failing self tests (Closes: #654604)
  * Run self test to run as LANG=C
  * Delete any self test failures on dh_clean
  * Update copyright header for released DEP5

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Thu, 03 May 2012 23:01:03 +0100

veusz (1.14-1) unstable; urgency=low

  * Update to upstream Veusz 1.14 (Closes: #648957)
  * Enable test suite in build, adding Build-Depends of xauth, xfonts-base
    and xvfb
  * Properly attribute copyright of pyqtdistutils.py file to Develer Srl
  * Add Break statement in control for veusz-helpers rather than use a
    Depends statement
  * Bumped Standards-Version to 3.9.2
  * Enabled dpkg-buildflags for build

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Sun, 04 Dec 2011 19:02:17 +0200

veusz (1.10-1) unstable; urgency=low

  * Initial release (Closes: #447524)

 -- Jeremy Sanders <jeremy@jeremysanders.net>  Wed, 30 Mar 2011 00:17:22 +0100
