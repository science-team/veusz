Source: veusz
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Sanders <jeremy@jeremysanders.net>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               dh-python,
               libqt5core5a,
               libqt5dbus5,
               libqt5gui5,
               libqt5svg5,
               libqt5widgets5,
               libqt5xml5,
               python3-all,
               python3-all-dev,
               python3-astropy,
               python3-h5py,
               python3-numpy,
               python3-pyqt5 (>= 5.15.0+dfsg-1+exp2),
               python3-pyqt5.qtsvg (>= 5.15.0+dfsg-1+exp2),
               python3-setuptools,
               python3-sipbuild,
               python3-sphinx,
               python3-tomli,
               pyqt5-dev (>= 5.15.0+dfsg-1+exp2),
               pyqt5-dev-tools,
               qt5-qmake,
               qtbase5-dev,
               sip-tools
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/veusz
Vcs-Git: https://salsa.debian.org/science-team/veusz.git
Homepage: https://veusz.github.io/
Rules-Requires-Root: no

Package: veusz
Architecture: all
Depends: python3-veusz (>= ${source:Version}),
         ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends}
Description: 2D and 3D scientific plotting application with graphical interface
 Veusz is a 2D and 3D scientific plotting and graphing package,
 designed to produce publication-ready PDF, SVG, Postscript and bitmap
 output. Veusz provides a GUI, command line and scripting interface
 (based on Python) to its plotting facilities. The plots are built
 using an object-based system to provide a consistent interface.
 .
 This package includes the main executable, documentation and examples.

Package: python3-veusz
Architecture: all
Section: python
Depends: python3-numpy,
         python3-pyqt5,
         python3-pyqt5.qtsvg,
         python3-veusz.helpers (>= ${source:Version}),
         ${misc:Depends},
         ${python3:Depends}
Suggests: ghostscript,
          python3-astropy,
          python3-dbus,
          python3-h5py
Description: 2D and 3D scientific plotting application (Python interface)
 Veusz is a 2D and 3D scientific plotting and graphing package,
 designed to produce publication-ready PDF, SVG, Postscript and bitmap
 output. Veusz provides a GUI, command line and scripting interface
 (based on Python) to its plotting facilities. The plots are built
 using an object-based system to provide a consistent interface.
 .
 This package includes the Python 3 plotting module.

Package: python3-veusz.helpers
Architecture: any
Section: python
Depends: python3-numpy,
         python3-pyqt5 (>= 5.15.0+dfsg-1+exp2),
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Suggests: python3-veusz (= ${source:Version})
Description: 2D and 3D scientific plotting application (binary modules)
 Veusz is a 2D and 3D scientific plotting and graphing package,
 designed to produce publication-ready PDF, SVG, Postscript and bitmap
 output. Veusz provides a GUI, command line and scripting interface
 (based on Python) to its plotting facilities. The plots are built
 using an object-based system to provide a consistent interface.
 .
 This package includes the architecture-specific parts of the Python 3 plotting
 module.
